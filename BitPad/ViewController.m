//
//  ViewController.m
//  BitPad
//
//  Created by Eleve on 12/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

@synthesize connexionViewController, gameViewController, gamekitHelper;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.connexionViewController = [[[ConnexionViewController alloc] initWithNibName:@"ConnexionViewController" bundle:nil] autorelease];
	[self.view insertSubview:self.connexionViewController.view atIndex:1];
	[self.connexionViewController viewWillAppear:NO];
    
    //Observers
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showGame:) name:kShowGameNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showConnection:) name:kShowConnectionNotification object:nil];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)showGame:(NSNotification *)notification
{
    if (self.gameViewController == nil)	{
		self.gameViewController = [[[GameViewController alloc] initWithNibName:@"GameViewController" bundle:nil] autorelease];
	}
    // prepare animation
	[UIView beginAnimations:@"anim" context:nil];
	[UIView setAnimationDuration:0.7];
	[UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.view cache:YES];
    
	[self.connexionViewController viewWillDisappear:YES];
	[self.gameViewController viewWillAppear:YES];
	
	[self.connexionViewController.view removeFromSuperview];
	[self.view insertSubview:self.gameViewController.view atIndex:0];
	
	[self.connexionViewController viewDidDisappear:YES];
	//[self.gameViewController viewDidAppear:YES];
	
	[UIView commitAnimations];
	
}

- (void)showConnection:(NSNotification *)notification {
    self.gamekitHelper = [[GamekitHelper alloc]init];
    [gamekitHelper lauchConnection];
}

- (void)dealloc {
    [super dealloc];
}

@end
