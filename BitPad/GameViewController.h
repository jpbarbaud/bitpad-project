//
//  GameViewController.h
//  BitPad
//
//  Created by Eleve on 13/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioServices.h>
#import "TouchableImageView.h"
#import "GamekitHelper.h"

@interface GameViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>;
   

@property (retain, nonatomic) IBOutlet UITableView *tableView;
@property (retain, nonatomic) IBOutlet TouchableImageView *doLow;
@property (retain, nonatomic) IBOutlet TouchableImageView *re;
@property (retain, nonatomic) IBOutlet TouchableImageView *mi;
@property (retain, nonatomic) IBOutlet TouchableImageView *fa;
@property (retain, nonatomic) IBOutlet TouchableImageView *sol;
@property (retain, nonatomic) IBOutlet TouchableImageView *la;
@property (retain, nonatomic) IBOutlet TouchableImageView *si;
@property (retain, nonatomic) IBOutlet TouchableImageView *doHigh;

@property (retain, nonatomic) NSMutableArray *notes;
@end