//
//  ViewController.h
//  BitPad
//
//  Created by Eleve on 12/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConnexionViewController.h"
#import "GameViewController.h"
#import "GamekitHelper.h"

@interface ViewController : UIViewController
@property (nonatomic, retain) ConnexionViewController *connexionViewController;
@property (nonatomic, retain) GameViewController *gameViewController;
@property (nonatomic, retain) GamekitHelper *gamekitHelper;
@end
