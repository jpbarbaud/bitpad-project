//
//  TouchableImageView.m
//  BitPad
//
//  Created by Eleve on 13/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TouchableImageView.h"

@implementation TouchableImageView
@synthesize delegate, name;

- (void)setName:(NSString *)newName
{
	[newName retain];
	[name release];
	name = newName;
}

- (id)initWithImage:(UIImage *)image
{
    self = [super initWithImage:image];
    return self;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	if(self.name == @"doLow" || self.name == @"doHigh")
	{
		self.image = [UIImage imageNamed:@"do_pressed"];
	}
	else
	{
		NSString *img = [NSString stringWithFormat:@"%@%@",self.name,@"_pressed"];
		self.image = [UIImage imageNamed:img];
	}
    [self.delegate imageWasTouched:self];
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	if(self.name == @"doLow" || self.name == @"doHigh")
	{
		self.image = [UIImage imageNamed:@"do"];
	}
	else
	{
		self.image = [UIImage imageNamed:self.name];
	}
}

-(void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(self.name == @"doLow" || self.name == @"doHigh")
	{
		self.image = [UIImage imageNamed:@"do"];
	}
	else
	{
		self.image = [UIImage imageNamed:self.name];
	}
}



- (void)dealloc
{
	[super dealloc];
}

@end