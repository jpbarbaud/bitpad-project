//
//  GamekitHelper.h
//  BitPad
//
//  Created by Eleve on 14/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Gamekit/Gamekit.h>

//Identify send/receive packet
typedef enum {
	NETWORK_ACK,					// no packet
	NETWORK_COINTOSS,				// decide who is going to be the server
	NETWORK_NOTES_EVENT,			// send notes
} packetCodes;

//identify game states
typedef enum {
	kStateStartGame,
	kStatePicker,
	kStateMultiplayer,
	kStateMultiplayerCointoss,
	kStateMultiplayerReconnect
} gameStates;

@interface GamekitHelper : UIViewController <GKPeerPickerControllerDelegate, GKSessionDelegate, UIAlertViewDelegate> {
    GKPeerPickerController *mpicker;
}

@property(nonatomic) NSInteger gameState; 
@property (nonatomic, retain) GKSession *gameSessions;
@property(nonatomic, copy)	 NSString	 *gamePeerId;
@property(nonatomic, retain) UIAlertView *connectionAlert;

- (void)lauchConnection;
- (void)invalidateSession:(GKSession *)session;
@end
