//
//  TouchableImageView.h
//  BitPad
//
//  Created by Eleve on 13/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TouchableImageDelegate;

@interface TouchableImageView : UIImageView

// public properties
@property (nonatomic, assign) IBOutlet id<TouchableImageDelegate> delegate;
@property (retain, nonatomic) NSString *name;

@end


@protocol TouchableImageDelegate <NSObject>

- (void)imageWasTouched:(TouchableImageView *)touchedImage;

@end
