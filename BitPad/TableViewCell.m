//
//  TableViewCell.m
//  BitPad
//
//  Created by Eleve on 14/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TableViewCell.h"

@implementation TableViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{   
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        //for (int i = 0; i < 6; i++) {
        //    UIView *myView = [[UIView alloc ] initWithFrame:CGRectMake(i * self.bounds.size.width / 6, 0, self.bounds.size.width / 6, self.bounds.size.height * .9)];
        //    myView.backgroundColor = [UIColor colorWithRed:i * 50/255.0 green:100.0/255.0 blue:100.0/255.0 alpha:1.0];
        //    [self addSubview:myView];
        //    [myView release];
        //}
    }
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    for (int i = 0; i < 6; i++) {
        UIView *myView = [[UIView alloc ] initWithFrame:CGRectMake(i * self.bounds.size.width / 6, 0, self.bounds.size.width / 6, self.bounds.size.height * .9)];
        myView.backgroundColor = [UIColor colorWithRed:i * 50/255.0 green:100.0/255.0 blue:100.0/255.0 alpha:1.0];
        [self addSubview:myView];
        [myView release];
    }
    
}

-(void)dealloc
{
    //todo
    [super dealloc];
}

@end
