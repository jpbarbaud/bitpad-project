//
//  GameViewController.m
//  BitPad
//
//  Created by Eleve on 13/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GameViewController.h"
#import "TableViewCell.h"

@implementation GameViewController

static NSString *CellClassName = @"TableViewCell";

@synthesize tableView;
@synthesize doLow, re, mi, fa, sol, la, si, doHigh;
@synthesize notes;

SystemSoundID sounds[8];

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.doLow.name = @"doLow";
    self.re.name = @"re";
    self.mi.name = @"mi";
    self.fa.name = @"fa";
    self.sol.name = @"sol";
    self.la.name = @"la";
    self.si.name = @"si";
    self.doHigh.name = @"doHigh";
	
   
	self.notes = [[[NSMutableArray alloc] init] autorelease];
	
	NSArray *notesName = [NSArray arrayWithObjects: self.doLow.name,
                          self.re.name,
                          self.mi.name,
                          self.fa.name,
                          self.sol.name,
                          self.la.name,
                          self.si.name,
                          self.doHigh.name,
                          nil];
	
	for (int i = 0 ; i < [notesName count] ; i++) {
		NSString *soundPath = [[NSBundle mainBundle] pathForResource:[notesName objectAtIndex:i] ofType:@"mp3"];
		CFURLRef soundURL = (CFURLRef)[NSURL fileURLWithPath:soundPath];
		AudioServicesCreateSystemSoundID(soundURL, &sounds[i]);
	}
	
}

- (void)viewDidUnload
{
    [self setDoLow:nil];
    [self setRe:nil];
    [self setMi:nil];
    [self setFa:nil];
    [self setSol:nil];
    [self setLa:nil];
    [self setSi:nil];
    [self setDoHigh:nil];
    //[self setTableView:nil];
	for (int i = 0 ; i <= 8 ; i++) {
		AudioServicesDisposeSystemSoundID(sounds[i]);
	}
	
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    [doLow release];
    [re release];
    [mi release];
    [fa release];
    [sol release];
    [la release];
    [si release];
    [doHigh release];
    //[tableView release];
    [super dealloc];
}

- (void)playSound:(NSInteger)noteIndex
{
	AudioServicesPlaySystemSound(sounds[(int)noteIndex]);
}


#pragma mark - Touchable image delegate

- (void)imageWasTouched:(TouchableImageView *)touchedImage
{
	
    [self.notes addObject:touchedImage.name];
    [self.tableView reloadData];
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.notes count]-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    [self playSound:touchedImage.tag];
    
    
}

#pragma mark - tableViewDelegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.notes count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView 
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellClassName];
    
    if (!cell)
    {
        cell = [[[TableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellClassName] autorelease];
    }
    //cell.fooData = [dataItems objectAtIndex:indexPath.row];
    
    return cell;
}
@end
