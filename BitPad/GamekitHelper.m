//
//  GamekitHelper.m
//  BitPad
//
//  Created by Eleve on 14/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GamekitHelper.h"

@implementation GamekitHelper
@synthesize gameState, gameSessions, gamePeerId, connectionAlert;

// GameKit Session ID for app
#define kTankSessionID @"bitpad"

#define kMaxPacketSize 1024

#pragma mark Lifecycle
- (void) dealloc {
    self.connectionAlert = nil;
    
    [super autorelease];
}

#pragma mark GKPeerPickerControllerDelegate Methods
- (void)peerPickerControllerDidCancel:(GKPeerPickerController *)picker { 
	// Peer Picker automatically dismisses on user cancel. No need to programmatically dismiss.
    
	// autorelease the picker. 
	picker.delegate = nil;
    [picker autorelease]; 
	
	// invalidate and release game session if one is around.
	if(self.gameSessions != nil)	{
		[self invalidateSession:self.gameSessions];
		self.gameSessions = nil;
	}
	
	// go back to start mode
	self.gameState = kStateStartGame;
} 

- (GKSession *)peerPickerController:(GKPeerPickerController *)picker sessionForConnectionType:(GKPeerPickerConnectionType)type{
    
	GKSession *session = [[GKSession alloc] initWithSessionID:kTankSessionID displayName:nil sessionMode:GKSessionModePeer];
    
    return [session autorelease];
}

-(void)peerPickerController:(GKPeerPickerController *)picker didConnectPeer:(NSString *)peerID toSession:(GKSession *)session {
    self.gamePeerId = peerID;
    self.gameSessions = session;
    self.gameSessions.delegate = self;
    [self.gameSessions setDataReceiveHandler:self withContext:NULL];
    
    [picker dismiss];
    picker.delegate = nil;
    [picker autorelease];
    
    //change game state
    self.gameState = kStateMultiplayerCointoss;
    
    //we are connected, we can display gameview
    [[NSNotificationCenter defaultCenter] postNotificationName:kShowGameNotification object:nil];
}

#pragma mark -
#pragma mark Session Related Methods

// session invalide
- (void)invalidateSession:(GKSession *)session {
	if(session != nil) {
		[session disconnectFromAllPeers]; 
		session.available = NO; 
		[session setDataReceiveHandler: nil withContext: NULL]; 
		session.delegate = nil; 
	}
}


#pragma mark GKSessionDelegate Methods
//en cas de changement de connection
- (void)session:(GKSession *)session peer:(NSString *)peerID didChangeState:(GKPeerConnectionState)state {
    if(self.gameState == kStatePicker) {
        return;
    }
    
    if(state == GKPeerStateDisconnected) {
        
        NSString *message = [NSString stringWithFormat:@"Could not reconnect with %@.", [session displayNameForPeer:peerID]];
        
        if((self.gameState == kStateMultiplayerReconnect) && self.connectionAlert && self.connectionAlert.visible) {
			self.connectionAlert.message = message;
		}else {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection perdu" message:message delegate:self cancelButtonTitle:@"Fin du jeu" otherButtonTitles:nil];
			self.connectionAlert = alert;
			[alert show];
			[alert release];
		}
		
		// go back to start mode
		self.gameState = kStateStartGame;
    }
}

//à la reception d'un paquet…
- (void)receiveData:(NSData *)data fromPeer:(NSString *)peer inSession:(GKSession *)session context:(void *)context { 
    
    static int lastPacketTime = -1;
	unsigned char *incomingPacket = (unsigned char *)[data bytes];
	int *pIntData = (int *)&incomingPacket[0];
    
    int packetTime = pIntData[0];
	int packetID = pIntData[1];
	if(packetTime < lastPacketTime && packetID != NETWORK_COINTOSS) {
		return;	
	}
    
    lastPacketTime = packetTime;
}

#pragma mark -
- (void)lauchConnection {
    mpicker = [[GKPeerPickerController alloc] init];
    mpicker.delegate = self;
    [mpicker show];
    NSLog(@":))");
    self.gameState = kStatePicker;
}

@end
